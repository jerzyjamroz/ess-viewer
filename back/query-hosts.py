from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from ics_hosts import get_hosts

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
       
@app.route('/q_get_hosts', methods = {'POST'})
def q_get_hosts():
    if request.method == 'POST':
        data = request.get_json()
        hosts = get_hosts(data['query'], data['desc'], data['type'])
        response = jsonify({'status': hosts['status'], 'hosts': hosts['hosts']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

if __name__ == '__main__':

    app.run(host='0.0.0.0', debug = True, port=5002)
