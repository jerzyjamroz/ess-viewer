import json
import xml.etree.ElementTree as ET
fbsPath = "/var/www/html/ess-viewer/backend/"
with open(fbsPath + 'fbs.json','r') as jsonFBS:
    listBreakdown = json.load(jsonFBS)

#Split into smaller chunks for better responsiveness given known tree structure
nodes = []

split = {"A01": "RFQ", "A02": "DTL", "W02": "MEBT", "A03": "Spoke", "A04": "MBL", "A05": "HBL", "A06": "TS2", "B01": "PBI", "G01": "Vacuum"}

for node in listBreakdown:
    tag = node['tag']
    if "=ESS.ACC" in tag or tag == "=ESS":
        nodes.append({"tag": tag, 'description': node['description']})

with open(fbsPath + 'accLight.json', 'w') as reducedFile:
    json.dump(nodes,reducedFile)

for el in split:
    nodes = []
    for node in listBreakdown:
        tag = node['tag']
        if "=ESS.ACC." + el in tag and node['essName'] is not None:
            nodes.append({"tag": tag, 'description': node['description'], 'essName': node['essName']})
    with open(fbsPath + "names" + split[el] + ".json", 'w') as reducedFile:
        json.dump(nodes,reducedFile)

nodes = []
for node in listBreakdown:
    if "=ESS.INFR" in node['tag'] or node['tag'] == "=ESS":
        nodes.append({"tag": node['tag'], 'description': node['description']})

with open(fbsPath + 'infrLight.json', 'w') as reducedFile:
    json.dump(nodes,reducedFile)

nodes = []
for node in listBreakdown:
    if "=ESS.NSS" in node['tag'] or node['tag'] == "=ESS":
        nodes.append({"tag": node['tag'], 'description': node['description']})

with open(fbsPath + 'nssLight.json', 'w') as reducedFile:
    json.dump(nodes,reducedFile)

nodes = []
for node in listBreakdown:
    if "=ESS.TS" in node['tag'] or node['tag'] == "=ESS":
        nodes.append({"tag": node['tag'], 'description': node['description']})

with open(fbsPath + 'tsLight.json', 'w') as reducedFile:
    json.dump(nodes,reducedFile)

nodes = []
for node in listBreakdown:
    if node['tag'].count('.') == 1 or node['tag'] == "=ESS":
        nodes.append({"tag": node['tag'], 'description': node['description']})

with open(fbsPath + 'top.json', 'w') as reducedFile:
    json.dump(nodes,reducedFile)


#Similar idea for XML data source
xmlTree = ET.parse(fbsPath + 'fbs.xml')
root = xmlTree.getroot()
rootAcc = ET.Element("root")
rootInfr = ET.Element("root")
list_xmlAll = []
list_xmlACC = []
list_xmlINFR = []
list_xmlTS = []
list_xmlNSS = []
list_xmlTop = []

for child in root:
    curTag = child.attrib['tag']
    try: 
        iso = child.attrib['isoClass']
    except Exception:
        iso = "undefined"
    lbs = "undefined"
    lbsDesc = "undefined"
    for el in child:
        if el.tag == "locatedIn":
            try:
                lbs = el.attrib['tag']
            except Exception:
                lbs = "undefined"
            try:
                lbsDesc = el.attrib['description']
            except Exception:
                lbsDesc = "undefined"

    list_xmlAll.append({'tag': curTag, 'isoClass': iso, 'lbs': lbs, 'lbsDesc': lbsDesc}) 
    if curTag.count('.') < 2:
        list_xmlTop.append({'tag': curTag, 'isoClass': iso, 'lbs': lbs, 'lbsDesc': lbsDesc}) 
    if "=ESS.ACC" in curTag:
        list_xmlACC.append({'tag': curTag, 'isoClass': iso, 'lbs': lbs, 'lbsDesc': lbsDesc}) 
    elif "=ESS.INFR" in curTag:
        list_xmlINFR.append({'tag': curTag, 'isoClass': iso, 'lbs': lbs, 'lbsDesc': lbsDesc}) 
    elif "=ESS.TS" in curTag:
        list_xmlTS.append({'tag': curTag, 'isoClass': iso, 'lbs': lbs, 'lbsDesc': lbsDesc}) 
    elif "=ESS.NSS" in curTag:
        list_xmlNSS.append({'tag': curTag, 'isoClass': iso, 'lbs': lbs, 'lbsDesc': lbsDesc}) 

with open(fbsPath + 'xmlAll.json','w') as jsonFile:
    json.dump(list_xmlAll,jsonFile)
with open(fbsPath + 'xmlTop.json','w') as jsonFile:
    json.dump(list_xmlTop,jsonFile)
with open(fbsPath + 'xmlACC.json','w') as jsonFile:
    json.dump(list_xmlACC,jsonFile)
with open(fbsPath + 'xmlINFR.json','w') as jsonFile:
    json.dump(list_xmlINFR,jsonFile)
with open(fbsPath + 'xmlTS.json','w') as jsonFile:
    json.dump(list_xmlTS,jsonFile)
with open(fbsPath + 'xmlNSS.json','w') as jsonFile:
    json.dump(list_xmlNSS,jsonFile)

tree = ET.parse(fbsPath + 'fbs.xml')
root = tree.getroot()

rootOutAcc = ET.Element('root')
rootOutINFR = ET.Element('root')
rootOutNSS = ET.Element('root')
rootOutTS = ET.Element('root')

for child in root:
    try:
        curTag = child.attrib['tag']
    except:
        curTag = "undefined"
    try:
        desc = child.attrib['description']
    except:
        desc = "undefined"

    childOut = ET.Element("fbs")
    childOut.set('tag', curTag)
    childOut.set('description', desc)
    for el in child:
        if el.tag == "documents":
            docsOut = ET.SubElement(childOut, "documents")
            for doc in el:
                docOut = ET.SubElement(docsOut, "document")
                for a in doc:
                    if a.text is None:
                        text = "undefined"
                    else:
                        text = a.text
                    docOut.set(a.tag,text)

    if "=ESS.ACC" in curTag:
        rootOutAcc.append(childOut)
    elif "=ESS.INFR" in curTag:
        rootOutINFR.append(childOut)
    elif "=ESS.NSS" in curTag:
        rootOutNSS.append(childOut)
    elif "=ESS.TS" in curTag:
        rootOutTS.append(childOut)
        
tree = ET.ElementTree(rootOutAcc)
tree.write(fbsPath + "docsACC.xml")
tree = ET.ElementTree(rootOutINFR)
tree.write(fbsPath + "docsINFR.xml")
tree = ET.ElementTree(rootOutNSS)
tree.write(fbsPath + "docsNSS.xml")
tree = ET.ElementTree(rootOutTS)
tree.write(fbsPath + "docsTS.xml")
