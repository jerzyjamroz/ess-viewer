import json
import sys
import os
import requests
import xml.etree.ElementTree as ET
import subprocess
from channelfinder import ChannelFinderClient
import urllib3
from epicsarchiver import ArchiverAppliance
import re
import fnmatch

''' 
Helper function to check if ESS prefix is present
'''
def checkPrefix(nodeFind):
    if len(nodeFind) == 0:
        return "=ESS"
    if nodeFind[0:3].upper() == "ESS":
        nodeFind = "=" + nodeFind
    nodeFind = nodeFind.upper()
    prefixFBS='=ESS'
    if nodeFind[-1] == ".":
        nodeFind = nodeFind[:-1]
    if prefixFBS not in nodeFind:
        return prefixFBS + '.' + nodeFind
    else:
        return nodeFind

'''
Helper function for list interrogation
'''    
def search_list(list,match):
    if match not in list:
        return -1
    for i, item in enumerate(list):
        if item == match:
            return i
'''
Helper to return description for a given tag
'''
def desc_from_tag(list,tag):
    for item in list:
        if item['tag'] == tag:
            return item['desc']
    return -1

'''
Helper function to discard leaf nodes
'''
def autumn(node, levels):

    cntLev = node.count('.')
    if levels > cntLev:
        return node

    tmp = node.split('.')
    parentNode = tmp[0]
    last = cntLev - levels + 1
    for i in tmp[1:last]:
        parentNode += '.' + i 
    return parentNode

'''
Breakdown structure nodes usually make little sense on their own.
Beneficial to append parent node information when displaying a node.
'''
def getAllParents(node):
    currentNode = node
    list_parents = list()
    searchDepth = currentNode.count('.')
    while searchDepth > 0:
        temp = currentNode.split('.')
        for i in range (0, searchDepth):
            if i == 0:
                currentNode = temp[0]
            else:
                currentNode += '.' + temp[i]
        searchDepth = currentNode.count('.')
        list_parents.append(currentNode)
    list_parents.sort()
    return list_parents

def getAllParentsDesc(listBreakdown,nodeFind):
    listParents = getAllParents(nodeFind)
    listParentDesc = []
    # Retrieve descriptions
    for node in listBreakdown:
        if node['tag'] in listParents:
            listParentDesc.append(node['tag'] + " ( " + node['description']  +  " ) ")
    return listParentDesc

def getDesc(node):
    desc = node['description'].replace('\n','')
    return ", <b>'description'</b>: " + desc

def getName(node):
    if node['essName'] is None:
        return ", '<b>essName'</b>: None defined."
    else:
        return ", <b>'essName':</b>" + node['essName']

def getID(node):
    if node['id'] is not None:
        return ' {' + node['id'] + ' }'
    else:
        return "Error retrieving ID."


def dropNewLines(str):
    return str.replace('\n', ' ')


'''
Helper function to get leaf node
'''
def getLeaf(node):
    temp = node.split('.')
    return temp[-1]

def getTokenVal(tokens, token, query):
    if query.count(token[0]) == len(token):
        posStart = query.find(token) + len(token)
        query = query[posStart:]
        posEnd = len(query)
        for test in tokens:
            if test != token and test in query:
                query = query[:query.find(test)]
        return query
    else:
        return ""

def getQuerySansTokens(tokens,query):
    for token in tokens:
        if token in query:
            posEnd = query.find(token) 
            query = query[:posEnd]
    return query

def getMetaData(node, withNames, withID):
    essName = ''
    essID = ''
    if withNames:
        if node['essName'] is None:
            essName = ", <b>'essName'</b>: None defined."
        else:
            essName = ", <b>'essName'</b>: " + node['essName']
    if withID:
        if node['id'] is None:
            essID = ", <b>'id'</b>: Not found."
        else:
            essID = ", <b>'id'</b>: " + node['id']
    return essName, essID
'''
*** getChildren *** 
Brief: FBS Tree viewer with advanced filtering options
nodeFind: FBS node to find
withNames: show ESS Names in output
withID: show ESS ID in output
byParent: group outputs by parent node (default is by level)
'''

srcPath = "/var/www/html/ess-viewer/backend/"
jsonFBS = srcPath + 'fbs.json'
jsonDescOnlyFBS = srcPath + 'fbsDescOnly.json'
xmlFBS = srcPath + 'fbs.xml'

def getChildren(nodeFind, withNames, withID):
    nodes = {'status': '','nodes': ''}
    nodeFind = checkPrefix(nodeFind)
    byParent = "++" in nodeFind or nodeFind[-1] == "*"
    searchDepthMax = False
    onlyParents = False

    if nodeFind[-1] == "*":
        nodeFind = nodeFind[:-1]
        searchDepthMax = True
    elif nodeFind[-1] == "$":
        onlyParents = True
        nodeFind = nodeFind[:-1]

    # '++' = search depth
    # '&' = match FBS leaf node 
    # '&&' = match FBS branch or leaf node 
    # '!' = exclude FBS leaf node 
    # '!!' = exclude FBS branch or leaf node 
    # '|' = match based on any part of node + meta-data
    # '~' = exclude based on any part of node + meta-data

    tokens = [ '++', '&', '!' ]
    searchDepth = getTokenVal(tokens, "++", nodeFind)
    if len(searchDepth) == 0:
        searchDepth = 1
    else:
        searchDepth = int(searchDepth)
    if searchDepthMax:
        searchDepth = 99

 
    withNames = withNames == "true"
    withID = withID == "true"
    jsonSelected = "fbs.json"
    if not withNames and not withID:
        if "=ESS.ACC" in nodeFind:
            jsonSelected = "accLight.json"
        elif "=ESS.INFR" in nodeFind:
            jsonSelected = "infrLight.json"
        elif "=ESS.NSS" in nodeFind:
            jsonSelected = "nssLight.json"
        elif "=ESS.TS" in nodeFind:
            jsonSelected = "tsLight.json"
        elif nodeFind.count('.') == 0 and searchDepth == 1 and "=ESS" in nodeFind:
            jsonSelected = "top.json"
        else:
            # Unrecognised root node
            nodes['status'] = "Root node '" + nodeFind + "'" + " unrecognised."        
            return nodes
                       
    matchAll = getTokenVal(tokens, "&", nodeFind)
    excludeAll = getTokenVal(tokens, "!", nodeFind)
    nodeFind = getQuerySansTokens(tokens,nodeFind) 
    list_include = matchAll.split(',')
    list_exclude = excludeAll.split(',')
    reMatch = False 
    reCompiled='Invalid FBS Node'

    if '*' in nodeFind:
        reMatch = True
        pattern = fnmatch.translate(nodeFind)
        reCompiled = re.compile(pattern) 

    essName = ''
    essID = ''
    # For formatting the matched nodes
    # Use '~' to order after '=' and '+' chars in sorting
    matchPrefix = "~> "

    fPath = os.path.dirname(os.path.realpath(__file__))

    with open(srcPath + jsonSelected) as inputFile:
        listBreakdown=json.load(inputFile)

    #list_matchedNodes = [tag, description]
    list_matchedNodes = list()
    # Compile a list of root nodes matching the wildcard pattern
    list_roots = list()
    prefixDesc = ", <b>'description':</b> "
    for node in listBreakdown:
        tag = node['tag']
        if re.search(reCompiled,tag):
            list_roots.append({'tag': tag, 'desc': node['description']})

    nodeCount_find = nodeFind.count('.')
    cntChild = 0
    for node in listBreakdown:
        noClash = 0
        tagFull = node['tag']
        hasRoot = False
        curRoot = '.....'
        exclude = False
        if reMatch:
            for rootTag in list_roots:
                if rootTag['tag'] in tagFull:
                    hasRoot = True
                    curRoot = rootTag['tag']
                    break
                    
        if nodeFind in tagFull or hasRoot:
            # Matched Root/Parent node
            if tagFull == nodeFind:
                essName, essID = getMetaData(node,withNames,withID)
                list_matchedNodes.append(tagFull + getDesc(node) + essName + essID)
            else:
                cntChild += 1
                nodeCount_tag = tagFull.count('.')
                nodeCount_root = curRoot.count('.')
                checkLevelTag = nodeCount_tag <= nodeCount_find + searchDepth
                checkLevelRoot = (nodeCount_root + searchDepth == nodeCount_tag)
                if curRoot == tagFull:
                    if excludeAll:
                        for el in list_exclude:
                            if el in tagFull: 
                                exclude = True
                                break 
                    if not exclude:
                        essName, essId = getMetaData(node,withNames, withID)
                        list_matchedNodes.append(curRoot + prefixDesc + desc_from_tag(list_roots,tagFull) + essName + essID)
                if not onlyParents and ((not reMatch and checkLevelTag) or (reMatch and checkLevelRoot)):
                    temp = tagFull.split('.')
                    parentNode = temp[0]
                    for el in temp[1:-1]:
                        parentNode += '.' + el
                    essName, essID = getMetaData(node, withNames, withID)
                    list_matchedNodes.append(tagFull + getDesc(node) + essName + essID + ", parent: " + parentNode)
    strOut=''        
    listNodes = []
    if byParent or reMatch:
        list_matchedNodes.sort()
    if len(list_matchedNodes) == 0:
        nodes['status'] = "No matches for " + nodeFind
        return nodes
    else:
        for node in list_matchedNodes:
            pos = node.find(", parent: ")
            matchNode = node[:pos]
            if reMatch:
                parentNode = node[pos+10:]
            else:
                parentNode = nodeFind
            if ', parent: =ESS' not in node:
                listNodes.append(node + "<br>")
            elif node == list_matchedNodes[-1]:
                listNodes.append(matchPrefix + matchNode.replace(parentNode + '.',''))
            else:
                listNodes.append(matchPrefix + matchNode.replace(parentNode + '.','') + '<br>')

    if len(list_include) > 0 or len(list_exclude) > 0:

        tmpInclude = []
        tmpExclude = []
        processInclude = len(list_include[0]) > 0
        processExclude = len(list_exclude[0]) > 0
        if not processInclude:
            tmpInclude = listNodes
        else:
            for el in listNodes:
                for includeNode in list_include:
                    if includeNode.upper() in el.upper() or el[0:4] == "=ESS":
                        tmpInclude.append(el)
                        break
        if processExclude:
            for el in tmpInclude:
                for excludeNode in list_exclude:
                    if excludeNode.upper() in el.upper():
                        tmpExclude.append(el)
                        break

    if len(tmpExclude) == 0:
        listNodes = tmpInclude
    else:
        listNodes = []
        for el in tmpInclude:
            if el not in tmpExclude:
                listNodes.append(el)

    nNodes = 0
    for el in listNodes:
        if "~> " in el:
            nNodes += 1 
    if reMatch:
        nodeString = str(nNodes) + " nodes matched." 
    else:
        if nodeFind == "=ESS":
          cntChild = "all"
        nodeString = str(nNodes) + " (of " + str(cntChild) + ") nodes matched."
    if nNodes == 1:
        nodeString = "One child node."
    elif nNodes < 1:
        nodeString = "No child nodes."
    nodes['status'] = nodeString 
    nodes['nodes'] = listNodes
    return nodes

def getDocs(findTag,download, titleOnly):
    docs = {'status': '', 'list': ''}
    simpleMatch = findTag.count('+') == 0 and findTag.count('*') == 0
    simpleMatch = simpleMatch and findTag.count('?') == 0

    findTag = checkPrefix(findTag)

    recursive = findTag[-1] == '*'
    if '++' in findTag:
        pos = findTag.find('++')
        try:
            levels = int(findTag[pos+2:])
        except:
            levels = 0
        findTag = findTag[:pos]
    else:
        levels = 0
    reCompiled=re.compile('')
    # initialisation
    reMatch = False
    list_unique = list()
    list_repeating = list()
    nDocs = 0
    url_link_CHESS='https://chess.esss.lu.se/enovia/common/emxNavigator.jsp?objectId='
    if recursive:
        findTag = findTag[:-1]
    elif '*' in findTag:
        pattern = fnmatch.translate(findTag)
        reCompiled = re.compile(pattern) 
        reMatch = True 
    
    selectedXML = "fbs.xml"
    if "=ESS.ACC" in findTag:
        selectedXML = "docsACC.xml"
    elif "=ESS.INFR" in findTag:
        selectedXML = "docsINFR.xml"
    elif "=ESS.NSS" in findTag:
        selectedXML = "docsNSS.xml"
    elif "=ESS.TS" in findTag:
        selectedXML = "docsTS.xml"

    tree = ET.parse(srcPath + selectedXML)
    strOut = ''
    root = tree.getroot()

    #allDocTags=["chess_number", "doc_filename", "doc_link" ,"doc_title", "link_id"]
    cntNodes = 0
    for childRoot in root:
        tag = childRoot.attrib['tag']
        levelDiff = tag.count('.') - findTag.count('.')
        if levelDiff >= 1 and findTag in tag:
            nestedMatch = autumn(tag, levels)
        else:
            nestedMatch = 'invalid'
        if nestedMatch in findTag or findTag == tag or (findTag in tag and recursive) or (reCompiled.search(tag) and reMatch):
            cntNodes += 1
            Documents = childRoot[-1]
            if not recursive: 
                strOut += "<br>" + tag + " ( " + childRoot.attrib['description']  + " )<br>"
            elif len(Documents) > 0:
                strOut += "<br>" + tag + " ( " + childRoot.attrib['description']  + " )<br>"
            if Documents.tag == "documents":
                if len(Documents) == 0 and not recursive:
                    strOut += "No documents attached to this node.<br>"
                for doc in Documents:
                    # Found a document is attached to this node
                    if doc.tag == 'document':
                        chess_number = ''
                        doc_filename = ''
                        doc_link = ''
                        doc_title = ''
                        doc_linkCHESS = ''
                        for DocAttr in doc.attrib:
                            if DocAttr == 'chess_number':
                                chess_number = doc.attrib[DocAttr]
                                nDocs += 1
                                if chess_number not in list_unique:
                                    list_unique.append(chess_number)
                                else:
                                    if chess_number not in list_repeating:
                                        list_repeating.append(chess_number)
                            if DocAttr == 'doc_filename':
                                    doc_filename = doc.attrib[DocAttr]
                            if DocAttr == 'doc_link':
                                    doc_link = doc.attrib[DocAttr]
                            if DocAttr == 'link_id':
                                    doc_linkCHESS = url_link_CHESS + doc.attrib[DocAttr]
                            if DocAttr == 'doc_title':
                                    doc_title= doc.attrib[DocAttr]

                        if titleOnly:
                            chess_number = ''
                        if chess_number is None:
                            chess_number = 'CHESS number not found'
                        if doc_link is None:
                            doc_link = 'Link not found'
                        if doc_filename is None:
                            doc_filename = 'Filename not found'
                        if doc_linkCHESS is None:
                            doc_linkCHESS = 'CHESS link not found'
                        if not titleOnly:
                            strTitle = " - " + doc_title  
                        else: 
                            strTitle = doc_title
                        doc_download = doc_link.replace("&inline=true","" )
                        linkInline = ' <a href = "' + doc_link + '">' + chess_number + strTitle + '</a>'
                        linkDownload = ' <a href = "' + doc_download + '"><b>Download</b></a>'
                        linkCHESS = ' <a href = "' + doc_linkCHESS + '">CHESS Object Link</a><br>' 
                        strOut += linkInline + linkDownload + "<br>"
            if simpleMatch:
                break
    # Remove line break from last line
    if len(strOut) > len('<br>'):
        if strOut[-4:] == '<br>':
            strOut = strOut[:-4]
    strNodesMatched = " " + str(cntNodes) + " FBS nodes matched."
    strRepeating = ''
    for el in list_repeating:
        strRepeating += el + ', '
    if len(strRepeating) > 0:
        strRepeating = "<br>Repeated documents: " + strRepeating
    if strRepeating[-2:] == ', ':
        strRepeating = strRepeating[:-2]
    if len(list_unique) == 1:
        docs['status'] = "One document found."
    else:
        docs['status'] = str(nDocs) + " documents found, of which " + str(len(list_unique)) + " are unique." + strNodesMatched + strRepeating
    docs['list'] = strOut 
    return docs

def getCables(nodeFind):
    listCables = []
    nodeFind = checkPrefix(nodeFind)
    #Special recursive case with trailing '*' wildcard
    recursive = nodeFind[-1] == '*'
    if recursive:
        nodeFind = nodeFind[:-1] 

    with open(jsonFBS, 'r') as infile:
        listFBS = json.load(infile)
    for node in listFBS:
        strOut = ''
        if node['cableName'] is not None:
            tag = node['tag']
            if nodeFind == tag or (nodeFind in tag and recursive):
                strOut = tag + ", <b>'cableName':</b> " + node['cableName']
                listCables.append(strOut);

    return listCables

def getDeviceListing(nodeFind):
    devices = []
    nodeFind = checkPrefix(nodeFind)
    selectedJSON = "fbs.json"
    if "=ESS.ACC.A01" in nodeFind:
        selectedJSON = "namesRFQ.json" 
    if "=ESS.ACC.A02" in nodeFind:
        selectedJSON = "namesDTL.json" 
    with open(srcPath + selectedJSON, 'r') as infile:
        listBreakdown = json.load(infile)

    for node in listBreakdown:
        tag = node['tag']
        if nodeFind in tag:
            if node['essName'] is not None:
                devices.append(node['essName'])
    return devices

def getDevices(nodeFind, withTypeDesc, withFBS):
    devices = {'status': '', 'list': []}
    include = None
    exclude = None
    if '&' in nodeFind:
        pos = nodeFind.find('&')
        include = nodeFind[pos + 1:]
        nodeFind = nodeFind[:pos]
    elif '!' in nodeFind:
        pos = nodeFind.find('!')
        exclude = nodeFind[pos + 1:]
        nodeFind = nodeFind[:pos]
    nodeFind = checkPrefix(nodeFind)
    reMatch = False
    reCompiled = ''
    list_roots = list()
    hasRoot = False
    if nodeFind[-1] == '*':
        devices['status'] = "Trailing wildcards not supported."
        return devices
    elif '*' in nodeFind:
        pattern = fnmatch.translate(nodeFind)
        reCompiled = re.compile(pattern) 
        reMatch = True

    url_naming = "https://naming.esss.lu.se"
    part_query = "/rest/parts/mnemonic/"
    selectedJSON = "fbs.json"
    if "=ESS.ACC.A01" in nodeFind:
        selectedJSON = "namesRFQ.json" 
    elif "=ESS.ACC.A02" in nodeFind:
        selectedJSON = "namesDTL.json"
    elif "=ESS.ACC.W02" in nodeFind:
        selectedJSON = "namesMEBT.json"
    elif "=ESS.ACC.A03" in nodeFind:
        selectedJSON = "namesSpk.json"
    elif "=ESS.ACC.A04" in nodeFind:
        selectedJSON = "namesMBL.json"
    elif "=ESS.ACC.A05" in nodeFind:
        selectedJSON = "namesHBL.json"
    elif "=ESS.ACC.A06" in nodeFind:
        selectedJSON = "namesTS2.json"
    elif "=ESS.ACC.B01" in nodeFind:
        selectedJSON = "namesPBI.json"
    elif "=ESS.ACC.G01" in nodeFind:
        selectedJSON = "namesVacuum.json"
    with open(srcPath + selectedJSON, 'r') as infile:
        listBreakdown = json.load(infile)
    # For wildcards matches we need to assemble a list of matched root nodes
    if reMatch:
        for node in listBreakdown:
            tag = node['tag']
            if re.search(reCompiled,tag):
                list_roots.append(tag) 
    #Use some primitive filtering of output results
    chkInclude = True
    chkExclude = False

    for node in listBreakdown:
        tag = node['tag']
        hasRoot = False
        if reMatch:
            for rootTag in list_roots:
                if rootTag in tag:
                    hasRoot = True
                    break
        if nodeFind in tag or (reMatch and hasRoot):
            if node['essName'] is not None:
                if include is not None:
                    chkInclude = include in node['essName']
                if exclude is not None:
                    chkExclude = exclude in node['essName']
                if chkInclude and not chkExclude:
                    dev = node['essName'] + ", <b>'Description'</b>: " + node['description']
                    if withTypeDesc:
                        if node['essName'].count(':') == 1:
                            devType = node['essName'].split(':',2)[1].split('-')[1]
                            response = requests.get(url_naming + part_query + devType)
                            root = ET.fromstring(response.content)
                            part = root.find('partElement')
                            desc = part.find('description')
                            if desc is None:
                                dev += ", <b>'Type description'</b>: No description."
                            else:
                                dev += ", <b>'Type description'</b>: " + desc.text
                    if withFBS:
                        dev += ", <b>'FBS'</b>: " + tag

                    devices['list'].append(dev)
    if len(devices['list']) > 0:
        devices['status'] = "Found " + str(len(devices['list'])) + " ESS Devices"
    else:
        devices['status'] = "No ESS Devices found underneath " + nodeFind
    return devices

def getDevicePVs(device):
    urllib3.disable_warnings()
    listPVs = list()

    url = "https://channelfinder.tn.esss.lu.se"
    cf = ChannelFinderClient(BaseURL = url) 
    # Then populate a list of all PVs belonging to the device
    channels = cf.find(name = device + ":*")
    for ch in channels:
        for prop in ch['properties']:
            if prop['name'] == 'pvStatus':
                active = prop['value'] == 'Active'
                break
        if active: 
            listPVs.append(ch['name']) 
        else:
            active = False
    return listPVs
    
def retrieveDesc(tag):
    desc = ""
    with open(jsonFBS) as inFile:
        listBreakdown = json.load(inFile)

    for node in listBreakdown:
        if node['tag'] == tag:
            desc = node['description']
            break

    return desc

def getPVs(nodeFind, onlyArc):
    # First get a list of essNames from the node and all of its children
    urllib3.disable_warnings()
    nodeFind = checkPrefix(nodeFind)

    selectedJSON = jsonFBS
    if onlyArc == "true":
        filePrefix = "arc"
    else:
        filePrefix = "pvs"
    selectedPVs = ""
    accSplit = {"A01": "RFQ", "A02": "DTL", "A03": "Spoke", "A04": "MBL", "A05": "HBL", "A06": "TS2", "W02": "MEBT", "B01.B01": "PBI-BCM", "B01.B02": "PBI-BLM", "B01.B03": "PBI-WS", "B01.B04": "PBI-FC", "B01.B05": "PBI-BPM", "G02": "Vacuum"}
    split = accSplit[nodeFind.replace("=ESS.ACC.","")]

    with open(srcPath + filePrefix + split, 'r') as inFile:
        listPVs = inFile.readlines()
    # Include only the relevant PVs
    devices = getDeviceListing(nodeFind)
    listOutput = []
    for pv in listPVs:
        for dev in devices: 
            if dev in pv:
                listOutput.append(pv)
                break
                
    return listOutput

def getParents(nodeFind):
    parentNodes = []
    nodeFind = checkPrefix(nodeFind)
    if nodeFind[-1] == "*":
        nodeFind = nodeFind[:-1]
    if nodeFind == "=ESS":
      parentNodes.append("ESS, ( ESS )")
      return parentNodes
    strOut = ''
    list_allParents = getAllParents(nodeFind)
    list_matchedParents = list()
    with open(jsonFBS, 'r') as infile:
        listNodes = json.load(infile)
    for node in listNodes:
        tag = node['tag']
        if tag in list_allParents:
            desc = node['description']
            list_matchedParents.append({'tag': tag, 'desc': desc})
    if len(list_matchedParents) > 0:
        for node in list_matchedParents:
            parentNodes.append(node['tag'] + " ( " + node['desc']  + " )")
        #Trim last line break
        return parentNodes
    else:
        return "No parent nodes found for node: " + nodeFind

def getCount(nodeFind):
    nodeFind = checkPrefix(nodeFind)
    with open(jsonFBS, 'r') as fbsJSON:
        listBreakdown=json.load(fbsJSON)
    
    countNodes = 0 
    for node in listBreakdown:
        if nodeFind in node['tag']:
            countNodes += 1 
    return countNodes

def get_xmlFields(list_tags,rootNode,withISO, withLBS, withLBS_Desc):
    list_fields = []
    selectedXMLs = "xmlAll.json"
    if rootNode == "=ESS":
        selectedXMLs = "xmlAll.json" 
    elif "=ESS.ACC" in rootNode:
        selectedXMLs = "xmlACC.json"
    elif "=ESS.INFR" in rootNode:
        selectedXMLs = "xmlINFR.json"
    elif "=ESS.NSS" in rootNode:
        selectedXMLs = "xmlNSS.json"
    elif "=ESS.TS" in rootNode:
        selectedXMLs = "xmlTS.json"
    with open(srcPath + selectedXMLs,'r') as jsonXMLs:
        listXMLs = json.load(jsonXMLs)
    
    nTags = len(list_tags)
    nMatches = 0
    for tag in list_tags:
        for el in listXMLs:
            curTag = el['tag'] 
            # Grab ISO fields for matching FBS tags
            if curTag == tag:
                nMatches += 1
                matchTag = True
                if withISO:
                    iso = el['isoClass']
                else:
                    iso = ""
                if withLBS:
                    lbs = el['lbs']
                else:
                    lbs = ""
                if withLBS_Desc:
                    lbsDesc = el['lbsDesc']
                else:
                    lbsDesc = ""
                list_fields.append(iso + ';' + lbs + ';' + lbsDesc)
                break
        if nMatches == nTags:
            break

    return list_fields
